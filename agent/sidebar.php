<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Agent Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,400;0,600;0,700;1,400;1,600&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.min.css">
  <link rel="stylesheet" type="text/css" href="css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="css/datepicker.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="js/datepicker.js"></script>
  <script type="text/javascript" src="js/datepicker.vi.js"></script>
</head>
<body>
  <div class="wrapper d-flex align-items-stretch">
    <nav id="sidebar">
      <div class="custom-menu">
        <button type="button" id="sidebarCollapse" class="btn btn-primary">
          <i class="fa fa-bars"></i>
          <span class="sr-only">Toggle Menu</span>
        </button>
      </div>
      <div class="agent-profile">
        <div class="d-flex align-items-center">
          <div class="mr-auto account-type">
            <i class="ion-ribbon-b"></i> Agent
          </div>
          <div class="agent-inform">
            <a href="inform.php"><i class="ion-android-notifications-none"></i></a>
          </div>
        </div>
        <div class="agent-images">
          <img src="images/profile.png" class="img-fluid" alt="">
        </div>
        <div class="agent-info">
          <h2>Lương Xuân Phước</h2>
          <div class="score">Điểm tích luỹ: <span class="number-score">500 điểm</span></div>
        </div>
      </div>
      <div class="agent-menu">
        <ul class="list-unstyled components mb-5">
          <li class="active">
            <a href="profile.php"><span class="fa fa-user-o mr-3"></span> Hồ sơ</a>
          </li>
          <li>
            <a href="upcoming-tour.php"><span class="fa fa-compass mr-3"></span> Tour sắp tới</a>
          </li>
          <li>
            <a href="history-tour.php"><span class="fa fa-clock-o mr-3"></span> Lịch sử tour</a>
          </li>
          <li>
            <a href="score-voucher.php"><span class="fa fa-gift mr-3"></span> Điểm & Voucher</a>
          </li>
          <li>
            <a href="#"><span class="fa fa-file-text-o mr-3"></span> Nhật ký hành trình của tôi</a>
          </li>
        </ul>
        <div class="logout-agent">
          <a href="">Đăng xuất</a>
        </div>
      </div>
    </nav>