<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Hồ sơ của bạn</h2>
    <div class="profile-content">
      <div class="avatar-box">
        <div class="agent-avatar">
          <img src="images/profile.png" class="img-fluid" alt="">
          <div class="change-avatar">
            <form action="">
              <input type="file" name="file" id="file-input" class="visuallyhidden">
            </form>
            <i class="ion-ios-camera-outline file-upload"></i>
          </div>
        </div>
        <div class="avatar-text">Ảnh đại diện</div>
      </div>
      <div class="infomation-form">
        <form class="" action="" method="POST">
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="surname">Họ</label>
              <input type="text" class="form-control" id="surname" value="Lương">
            </div>
            <div class="form-group col-sm-6">
              <label for="agent-name">Tên</label>
              <input type="text" class="form-control" id="agent-name" value="Xuân Phước">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="country">Quốc tịch</label>
              <select id="country" class="form-control">
                <option selected="">Việt Nam</option>
                <option>Thailand</option>
                <option>China</option>
                <option>Australia</option>
              </select>
            </div>
            <div class="form-group col-sm-6">
              <label for="agent-gender">Giới tính</label>
              <input type="text" class="form-control" id="agent-gender" value="Nam">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="agent-email">Email</label>
              <input type="email" class="form-control" id="agent-email" value="phuoclx129@gmail.com">
            </div>
            <div class="form-group col-sm-6">
              <label for="agent-phone">Số điện thoại</label>
              <input type="number" class="form-control" id="agent-phone" value="0973950482">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-sm-12 text-center">
              <button type="submit" class="agent-update">Cập nhật</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.file-upload').on('click', function(e) {
    e.preventDefault();
    $('#file-input').trigger('click');
  });
</script>
<?php include 'footer.php'?>