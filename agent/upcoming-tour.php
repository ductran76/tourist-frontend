<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Tour sắp tới</h2>
    <div class="upcoming-tour-list">
     <div class="upcoming-tour-item">
       <div class="row">
         <div class="col-sm-6">
           <div class="upcoming-tour-info">
             <div class="upcoming-tour-time">13/05/2020 - 16/05/2020</div>
             <div class="upcoming-tour-name d-flex align-items-center">
               <h3><span class="start-point">Hà Nội</span> <i class="ion-arrow-right-c"></i> <span class="end-point">Hà Nội, Ninh Bình</span></h3>
             </div>
             <div class="payment-status d-flex">
               <div class="upcoming-tour-price">2,210,000VNĐ</div>
               <div class="upcoming-tour-status paid">Đã thanh toán</div>
             </div>
           </div>
         </div>
         <div class="col-sm-6 right-info">
           <div class="guide-rental-info">
            <div class="guide-rental-text">Hướng dẫn viên (1)</div>
            <div class="d-flex">
              <div class="guide-detail-image">
                <div class="image-guide">
                  <img src="images/guidedetail.png" class="img-fluid" alt="">
                </div>
              </div>
              <div class="guide-detail-info">
                <h3>Lý Thành Cơ</h3>
                <div class="guide-level">
                  <ul class="mb-0 list-unstyled">
                    <li>27 tuổi</li>
                    <li>Hướng dẫn viên chuyên nghiệp - Hạng B</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 text-center">
          <div class="view-detail">
            <a href="#" data-izimodal-open="#has-paid" data-izimodal-transitionin="comingIn">Xem chi tiết</a>
          </div>
        </div>
      </div>
    </div>
    <div class="upcoming-tour-item">
     <div class="row">
       <div class="col-sm-6">
         <div class="upcoming-tour-info">
           <div class="upcoming-tour-time">22/06/2020 - 25/06/2020</div>
           <div class="upcoming-tour-name d-flex align-items-center">
             <h3><span class="start-point">Hà Nội</span> <i class="ion-arrow-right-c"></i> <span class="end-point">TP. Hồ Chí Minh, Đà Lạt</span></h3>
           </div>
           <div class="payment-status d-flex">
             <div class="upcoming-tour-price">4,410,000VNĐ</div>
             <div class="upcoming-tour-status waiting">Chờ xác nhận</div>
           </div>
         </div>
       </div>
       <div class="col-sm-6 right-info">
         <div class="guide-rental-info">
          <div class="guide-rental-text">Hướng dẫn viên (2)</div>
          <div class="d-flex">
            <div class="guide-detail-image">
              <div class="image-guide">
                <img src="images/guidedetail.png" class="img-fluid" alt="">
              </div>
            </div>
            <div class="guide-detail-info">
              <h3>Lý Thành Cơ</h3>
              <div class="guide-level">
                <ul class="mb-0 list-unstyled">
                  <li>27 tuổi</li>
                  <li>Hướng dẫn viên chuyên nghiệp - Hạng B</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12 text-center">
        <div class="view-detail">
          <a href="#" data-izimodal-open="#has-waiting" data-izimodal-transitionin="comingIn">Xem chi tiết</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<div id="has-paid" class="modal-izi"> 
  <button data-izimodal-close="" class="icon-close" data-izimodal-transitionout="comingOut"></button>
  <div class="has-paid-content">
    <h2 class="title-pop">Chi tiết chuyến</h2>
    <div class="trip-details">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin chuyến đi</h3>
          <div class="table-responsive trip-details-info">
            <table class="table">
              <tr>
                <td>Điểm đón</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Điểm đến</td>
                <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
              </tr>
              <tr>
                <td>Điểm kết thúc</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Thời gian thuê</td>
                <td>13/05/2020 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Loại xe</td>
                <td>Xe Vios 2018</td>
              </tr>
              <tr>
                <td>Đón sân bay</td>
                <td>08:30 - 13/05/2020</td>
              </tr>
              <tr>
                <td>Tiễn sân bay</td>
                <td>08:30 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Trạng thái</td>
                <td><span class="upcoming-tour-status paid ml-0">Đã thanh toán</span></td>
              </tr>
            </table>
          </div>

          <div class="payment-invoice">
            <h3 class="title-item">Thanh toán</h3>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Phí cao tốc 2 chiều</td>
                  <td>105,000 * 2 = 210,000VNĐ</td>
                </tr>
                <tr>
                  <td>Công tác phí HDV</td>
                  <td>500,000 * 3 = 1,500,000VNĐ</td>
                </tr>
                <tr>
                  <td>Tiễn sân bay</td>
                  <td>500,000VNĐ</td>
                </tr>
                <tr class="line-up">
                  <td>Tổng thanh toán:</td>
                  <td><span class="total-bill">2,210,000VNĐ</span></td>
                </tr>
                <tr class="line-down">
                  <td>Chuyển khoản</td>
                  <td>2,210,000VNĐ</td>
                </tr>
                <tr>
                  <td>Tích luỹ điểm</td>
                  <td>220 điểm</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin hướng dẫn viên</h3>
          <div class="tour-guide-info">
            <div class="d-flex">
              <div class="guide-detail-image">
                <div class="image-guide">
                  <img src="images/guidedetail.png" class="img-fluid" alt="">
                </div>
              </div>
              <div class="guide-detail-info d-flex align-items-start flex-column">
                <div class="mb-auto">
                  <h3>Lý Thành Cơ</h3>
                  <div class="guide-level">
                    <ul class="mb-0 list-unstyled">
                      <li>Mã HDV: 398603</li>
                      <li>Hướng dẫn viên chuyên nghiệp - Hạng B</li>
                    </ul>
                  </div>
                </div>
                <div class="guide-email"><i class="ion-ios-email-outline"></i> colt_tourguide@gmail.com</div>
              </div>
            </div>
          </div>
          <div class="form-group-icon-note">
            <input type="text" class="form-control" value="Đón mình ở khách sạn Mường Thanh Cầu Giấy" disabled name="">
          </div>
          <div class="view-list-guest">
            <a href="">Xem danh sách</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="has-waiting" class="modal-izi">
  <button data-izimodal-close="" class="icon-close" data-izimodal-transitionout="comingOut"></button>
  <div class="has-waiting-content">
    <h2 class="title-pop">Chi tiết chuyến</h2>
    <div class="trip-details">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin chuyến đi</h3>
          <div class="table-responsive trip-details-info">
            <table class="table">
              <tr>
                <td>Điểm đón</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Điểm đến</td>
                <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
              </tr>
              <tr>
                <td>Điểm kết thúc</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Thời gian thuê</td>
                <td>13/05/2020 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Loại xe</td>
                <td>Xe Vios 2018</td>
              </tr>
              <tr>
                <td>Đón sân bay</td>
                <td>08:30 - 13/05/2020</td>
              </tr>
              <tr>
                <td>Tiễn sân bay</td>
                <td>08:30 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Trạng thái</td>
                <td><span class="upcoming-tour-status waiting ml-0">Chờ xác nhận</span></td>
              </tr>
            </table>
          </div>

          <div class="payment-invoice">
            <h3 class="title-item">Thanh toán</h3>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Phí cao tốc 2 chiều</td>
                  <td>105,000 * 2 = 210,000VNĐ</td>
                </tr>
                <tr>
                  <td>Công tác phí HDV</td>
                  <td>500,000 * 3 = 1,500,000VNĐ</td>
                </tr>
                <tr>
                  <td>Tiễn sân bay</td>
                  <td>500,000VNĐ</td>
                </tr>
                <tr class="line-up">
                  <td>Tổng thanh toán:</td>
                  <td><span class="total-bill">2,210,000VNĐ</span></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin hướng dẫn viên</h3>
          <div class="tour-guide-info">
            <div class="d-flex">
              <div class="guide-detail-image">
                <div class="image-guide">
                  <img src="images/guidedetail.png" class="img-fluid" alt="">
                </div>
              </div>
              <div class="guide-detail-info d-flex align-items-start flex-column">
                <div class="mb-auto">
                  <h3>Lý Thành Cơ</h3>
                  <div class="guide-level">
                    <ul class="mb-0 list-unstyled">
                      <li>Mã HDV: 398603</li>
                      <li>Hướng dẫn viên chuyên nghiệp - Hạng B</li>
                    </ul>
                  </div>
                </div>
                <div class="guide-email"><i class="ion-ios-email-outline"></i> Chưa cập nhật</div>
              </div>
            </div>
          </div>
          <div class="tour-guide-info">
            <div class="d-flex">
              <div class="guide-detail-image">
                <div class="image-guide">
                  <img src="images/guidedetail2.png" class="img-fluid" alt="">
                </div>
              </div>
              <div class="guide-detail-info d-flex align-items-start flex-column">
                <div class="mb-auto">
                  <h3>Dương Thành Nam</h3>
                  <div class="guide-level">
                    <ul class="mb-0 list-unstyled">
                      <li>Mã HDV: 398603</li>
                      <li>Hướng dẫn viên chuyên nghiệp - Hạng A</li>
                    </ul>
                  </div>
                </div>
                <div class="guide-email"><i class="ion-ios-email-outline"></i> Chưa cập nhật</div>
              </div>
            </div>
          </div>
          <div class="form-group-icon-note">
            <input type="text" class="form-control" value="Đón mình ở khách sạn Mường Thanh Cầu Giấy" disabled name="">
          </div>
          <div class="view-list-guest">
            <a href="">Xem danh sách</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $("#has-paid, #has-waiting").iziModal({
      padding: 25
    });
    $("#has-paid").iziModal('setWidth', 950);
    $("#has-waiting").iziModal('setWidth', 950);
  })
</script>
<?php include 'footer.php'?>