<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Thông báo</h2>
    <div class="">
      <div class="eduction-table table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">STT</th>
              <th scope="col">Nội dung</th>
              <th scope="col">Thời gian</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Chuyến đi Hà Nội -> Ninh Bình ngày 14/05/2020 - 17/05/2020 đã được xác nhận</td>
              <td>13/05/2020 - 08:30</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Chương trình khuyến mại nhân dịp tuần lễ vàng</td>
              <td>11/05/2020 - 08:30</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php'?>