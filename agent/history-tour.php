<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Lịch sử tour</h2>
    <div class="">
       <div class="eduction-table table-responsive">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Điểm đến</th>
                <th scope="col">Hướng dẫn viên</th>
                <th scope="col">Thời gian</th>
                <th scope="col">Chi phí</th>
                <th scope="col">Danh sách khách</th>
                <th scope="col">Điểm</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
                <td>Lý Thành Cơ</td>
                <td>13/05/2020 - 16/05/2020</td>
                <td>2,210,000VNĐ</td>
                <td><a href="">Xem danh sách</a></td>
                <td>+220</td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td><span class="destination">Đà Lạt</span> <span class="destination">TP HCM</span></td>
                <td>Đặng Thuỳ Châm</td>
                <td>21/01/2020 - 26/01/2020</td>
                <td>3,300,000VNĐ</td>
                <td><a href="">Xem danh sách</a></td>
                <td>+330</td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td><span class="destination">Quy Nhơn</span></td>
                <td>Cao Thuỳ Dương</td>
                <td>16/07/2019 - 20/07/2020</td>
                <td>2,400,000VNĐ</td>
                <td><a href="">Xem danh sách</a></td>
                <td>+240</td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
  </div>
</div>

<?php include 'footer.php'?>