<?php include 'sidebar.php'?>
<style type="text/css">
.-other-month- {
  visibility: hidden;
}
</style>

<div id="content">
  <div class="agent-page-title">
    <h2>Lịch bận</h2>
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="form-group row">
          <div class="col-sm-4">
            <div class="form-group-icon-calendar">
              <input type="text" name="morning-choose" data-language="vi" class="datetimepicker form-control" placeholder="Chọn ngày bận buổi sáng" id="morning-choose">
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group-icon-calendar">
              <input type="text" name="afternoon-choose" data-multiple-dates="15" data-multiple-dates-separator=", " data-language="vi" class="datetimepicker form-control" placeholder="Chọn ngày bận buổi chiều" id="afternoon-choose">
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group-icon-calendar">
              <input type="text" name="allday-choose" data-multiple-dates="15" data-multiple-dates-separator=", " data-language="vi" class="datetimepicker form-control" placeholder="Chọn ngày bận cả ngày" id="allday-choose">
            </div>
          </div>
        </div>
        <div class="col-sm-12 text-center mb-5">
          <button type="" class="btn-update"">Cập nhật</button>
        </div>
      </div>
    </div>
    <div class="row time-busy">
      <div class="col-sm-4">
        <div class="datetimepicker monthToday"></div>
      </div>
      <div class="col-sm-4">
        <div class="datetimepicker nextMonth"></div>
      </div>
      <div class="col-sm-4">
        <div class="datetimepicker next2Month"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="d-flex justify-content-center explain">
          <div class="freeday">
            <span class=""></span> Ngày rảnh
          </div>
          <div class="morning-busy">
            <span class=""></span> Bận từ 00:00 đến 11:59 AM
          </div>
          <div class="afternoon-busy">
            <span class=""></span> Bận từ 12:00 PM đến 11:59 PM
          </div>
          <div class="allday-busy">
            <span class=""></span> Bận cả ngày
          </div>
        </div>
      </div>
     <!--  <div class="col-sm-12 text-center">
        <button type="" class="btn-update">Cập nhật</button>
      </div> -->
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="choose-days">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button> 
      </div>
      <div class="modal-body">
        <div class="container ">
          <div class="holder">
            <div class="row mb-1">
              <div class="col text-center">
                <h2>Chọn thời gian bận</h2>
              </div>
            </div>
            <form action="#" class="customRadio customCheckbox m-0 p-0">
              <div class="row mb-0">
                <div class="row justify-content-start">
                  <div class="col-12">
                    <div class="item-choose"> 
                      <input type="radio" name="textEditor" id="busy-on-morning"> 
                      <label for="busy-on-morning">Bận từ 00:00 - 11:59 AM</label> 
                    </div>
                    <div class="item-choose"> 
                      <input type="radio" name="textEditor" id="busy-on-afternoon"> 
                      <label for="busy-on-afternoon">Bận từ 12:00 - 11:59 PM</label> 
                    </div>
                    <div class="item-choose"> 
                      <input type="radio" name="textEditor" id="busy-on-allday"> 
                      <label for="busy-on-allday">Bận cả ngày</label> 
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer pt-0 mt-0 pb-5 text-center">
        <button type="submit" class="btn-update submit-choose">OK</button> 
      </div>
    </div>
  </div> -->

  <script type="text/javascript">
    $(document).ready(function () {
      var allday_busy = [],
      morning_busy = [],
      afternoon_busy = [],
      months = ['01','02','03','04','05','06','07','08','09','10','11','12'],
      $picker = $('.datetimepicker');

      $(".monthToday").datepicker({
        minDate: new Date()
      })
      var nextmonth = $('.nextMonth').datepicker().data('datepicker');
      nextmonth.date = new Date('10/01/2020');
      var next2month = $('.next2Month').datepicker().data('datepicker');
      next2month.date = new Date('11/01/2020');

      $picker.datepicker({
        language: "vi",
        dateFormat: 'dd/mm/yyyy', 
        moveToOtherMonthsOnSelect: false,
        disableNavWhenOutOfRange: true,
        onSelect: function(formattedDate, date, inst) {
          $(inst.el).trigger('change');
        },
        onRenderCell: function (date, cellType) {
          var myDate = (date.getDate()+'/'+months[date.getMonth()]+'/'+date.getFullYear());
          if (cellType == 'day' && allday_busy.indexOf(myDate) != -1) {
            return {
              classes: 'busy-allday',
              disabled: true
            }
          }
          if (cellType == 'day' && morning_busy.indexOf(myDate) != -1) {
            return {
              classes: 'morning-busy'
            }
          }
          if (cellType == 'day' && afternoon_busy.indexOf(myDate) != -1) {
            return {
              classes: 'afternoon-busy'
            }
          }
        }
      })

      $(".btn-update").click(function() {
        var first, second, third;
        first = $(".datetimepicker[name=morning-choose]").val();
        second = $(".datetimepicker[name=afternoon-choose]").val();
        third = $(".datetimepicker[name=allday-choose]").val();

        morning_busy.push(first);
        afternoon_busy.push(String(second));
        allday_busy.push(String(third));
        $picker.datepicker();
     });
    })
  </script>
  <?php include 'footer.php'?>