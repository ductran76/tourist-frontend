<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Tour sắp tới</h2>
    <div class="eduction-table incoming table-responsive">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">STT</th>
            <th scope="col">Điểm đón</th>
            <th scope="col">Điểm đến</th>
            <th scope="col">Thời gian</th>
            <th scope="col">Chi phí</th>
            <th scope="col">Trạng thái</th>
          </tr>
        </thead>
        <tbody>
          <tr data-izimodal-open="#has-paid" data-izimodal-transitionin="comingIn">
            <th scope="row">1</th>
            <td>Hà Nội</td>
            <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
            <td>13/05/2020 - 16/05/2020</td>
            <td>2,210,000VNĐ</td>
            <td><div class="upcoming-tour-status paid">Đã thanh toán</div></td>
          </tr>
          <tr data-izimodal-open="#has-waiting" data-izimodal-transitionin="comingIn">
            <th scope="row">2</th>
            <td>Hà Nội</td>
            <td><span class="destination">Đà Lạt</span> <span class="destination">TP HCM</span></td>
            <td>21/01/2020 - 26/01/2020</td>
            <td>3,300,000VNĐ</td>
            <td><div class="upcoming-tour-status waiting">Chờ xác nhận</div></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div id="has-paid" class="modal-izi"> 
  <button data-izimodal-close="" class="icon-close" data-izimodal-transitionout="comingOut"></button>
  <div class="has-paid-content">
    <h2 class="title-pop">Chi tiết chuyến</h2>
    <div class="trip-details">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin chuyến đi</h3>
          <div class="table-responsive trip-details-info">
            <table class="table">
              <tr>
                <td>Điểm đón</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Điểm đến</td>
                <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
              </tr>
              <tr>
                <td>Điểm kết thúc</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Thời gian thuê</td>
                <td>13/05/2020 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Loại xe</td>
                <td>Xe Vios 2018</td>
              </tr>
              <tr>
                <td>Đón sân bay</td>
                <td>08:30 - 13/05/2020</td>
              </tr>
              <tr>
                <td>Tiễn sân bay</td>
                <td>08:30 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Trạng thái</td>
                <td><span class="upcoming-tour-status paid ml-0">Đã thanh toán</span></td>
              </tr>
            </table>
          </div>

          <div class="payment-invoice">
            <h3 class="title-item">Thanh toán</h3>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Phí cao tốc 2 chiều</td>
                  <td>105,000 * 2 = 210,000VNĐ</td>
                </tr>
                <tr>
                  <td>Công tác phí HDV</td>
                  <td>500,000 * 3 = 1,500,000VNĐ</td>
                </tr>
                <tr>
                  <td>Tiễn sân bay</td>
                  <td>500,000VNĐ</td>
                </tr>
                <tr class="line-up">
                  <td>Tổng thanh toán:</td>
                  <td><span class="total-bill">2,210,000VNĐ</span></td>
                </tr>
                <tr class="line-down">
                  <td>Chuyển khoản</td>
                  <td>2,210,000VNĐ</td>
                </tr>
                <tr>
                  <td>Tích luỹ điểm</td>
                  <td>220 điểm</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin khách hàng</h3>
          <div class="tour-guide-info">
            <div class="d-flex">
              <div class="guide-detail-image">
                <div class="image-guide">
                  <img src="images/customor.png" class="img-fluid" alt="">
                </div>
              </div>
              <div class="guide-detail-info d-flex align-items-start flex-column">
                <div class="mb-auto">
                  <h3>Lương Xuân Phước</h3>
                  <div class="guide-level">
                    <ul class="mb-0 list-unstyled">
                      <li>Giới tính: Nam</li>
                      <li>Quốc tịch: Việt Nam</li>
                    </ul>
                  </div>
                </div>
                <div class="guide-contact">
                  <div class="guide-phone"><i class="ion-ios-telephone-outline"></i> 0973950482</div>
                  <div class="guide-email"><i class="ion-ios-email-outline"></i> phuoclx129@gmail.com</div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group-icon-note">
            <input type="text" class="form-control" value="Đón mình ở khách sạn Mường Thanh Cầu Giấy" disabled name="">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="has-waiting" class="modal-izi">
  <button data-izimodal-close="" class="icon-close" data-izimodal-transitionout="bounceOutDown"></button>
  <div class="has-waiting-content">
    <h2 class="title-pop">Chi tiết chuyến</h2>
    <div class="trip-details">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin chuyến đi</h3>
          <div class="table-responsive trip-details-info">
            <table class="table">
              <tr>
                <td>Điểm đón</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Điểm đến</td>
                <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
              </tr>
              <tr>
                <td>Điểm kết thúc</td>
                <td><span class="destination">Hà Nội</span></td>
              </tr>
              <tr>
                <td>Thời gian thuê</td>
                <td>13/05/2020 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Loại xe</td>
                <td>Xe Vios 2018</td>
              </tr>
              <tr>
                <td>Đón sân bay</td>
                <td>08:30 - 13/05/2020</td>
              </tr>
              <tr>
                <td>Tiễn sân bay</td>
                <td>08:30 - 16/05/2020</td>
              </tr>
              <tr>
                <td>Trạng thái</td>
                <td><span class="upcoming-tour-status waiting ml-0">Chờ xác nhận</span></td>
              </tr>
            </table>
          </div>

          <div class="payment-invoice">
            <h3 class="title-item">Thanh toán</h3>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Phí cao tốc 2 chiều</td>
                  <td>105,000 * 2 = 210,000VNĐ</td>
                </tr>
                <tr>
                  <td>Công tác phí HDV</td>
                  <td>500,000 * 3 = 1,500,000VNĐ</td>
                </tr>
                <tr>
                  <td>Tiễn sân bay</td>
                  <td>500,000VNĐ</td>
                </tr>
                <tr class="line-up">
                  <td>Tổng thanh toán:</td>
                  <td><span class="total-bill">2,210,000VNĐ</span></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <h3 class="title-item">Thông tin khách hàng</h3>
          <div class="tour-guide-info">
            <div class="d-flex">
              <div class="guide-detail-image">
                <div class="image-guide">
                  <img src="images/customor.png" class="img-fluid" alt="">
                </div>
              </div>
              <div class="guide-detail-info d-flex align-items-start flex-column">
                <div class="mb-auto">
                  <h3>Lương Xuân Phước</h3>
                  <div class="guide-level">
                    <ul class="mb-0 list-unstyled">
                      <li>Giới tính: Nam</li>
                      <li>Quốc tịch: Việt Nam</li>
                    </ul>
                  </div>
                </div>
                <div class="guide-contact">
                  <div class="guide-phone"><i class="ion-ios-telephone-outline"></i> Chưa cập nhật</div>
                  <div class="guide-email"><i class="ion-ios-email-outline"></i> Chưa cập nhật</div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group-icon-note customor-note">
            <input type="text" class="form-control" value="Đón mình ở khách sạn Mường Thanh Cầu Giấy" disabled name="">
          </div>

          <h3 class="title-item">Hướng dẫn viên đồng hành</h3>
          <div class="tour-guide-info">
            <div class="d-flex">
              <div class="guide-detail-image">
                <div class="image-guide">
                  <img src="images/guidedetail2.png" class="img-fluid" alt="">
                </div>
              </div>
              <div class="guide-detail-info d-flex align-items-start flex-column">
                <div class="mb-auto">
                  <h3>Dương Thành Nam</h3>
                  <div class="guide-level">
                    <ul class="mb-0 list-unstyled">
                      <li>Mã HDV: 398603</li>
                      <li>Hướng dẫn viên chuyên nghiệp - Hạng A</li>
                    </ul>
                  </div>
                </div>
                <div class="guide-contact">
                  <div class="guide-phone"><i class="ion-ios-telephone-outline"></i> 0973950482</div>
                  <div class="guide-email"><i class="ion-ios-email-outline"></i> namdt_tourguide@gmail.com</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $("#has-paid, #has-waiting").iziModal({
      padding: 25
    });
    $("#has-paid").iziModal('setWidth', 950);
    $("#has-waiting").iziModal('setWidth', 950);
  })
</script>
<?php include 'footer.php'?>