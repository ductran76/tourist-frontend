<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Điểm & Voucher</h2>
    <div class="nav-tabs-wrapper text-center">
      <ul class="nav nav-tabs criteria-filter" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" role="tab" href="#agent-score" aria-selected="true" aria-controls="agent-score" id="agent-score-tab">Quy đổi</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" role="tab" href="#agent-money" aria-selected="false" aria-controls="agent-money" id="agent-money-tab">Voucher của tôi</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" role="tab" href="#agent-voucher" aria-selected="false" aria-controls="agent-voucher" id="agent-voucher-tab">Sao kê điểm</a>
        </li>
      </ul>
    </div>
    <div class="tab-content py-4">
      <div id="agent-score" class="tab-pane active" role="tabpanel" aria-labelledby="agent-score-tab">
        <div class="agent-score-content">
          <h3>Điểm tích luỹ: 500 điểm</h3>
          <div class="type-change-text">Đổi điểm lấy tiền hoặc voucher</div>
          <div class="form-exchange">
            <form class="" action="" method="POST">
              <div class="row">
                <div class="form-group col-sm-3">
                  <label for="exchange-score">Điểm</label>
                  <input type="text" class="form-control" id="exchange-score" value="500">
                </div>
                <div class="form-group col-sm-1 exchange-icon">
                  <i class="ion-arrow-swap"></i>
                </div>
                <div class="form-group has-icon exchange-choose col-sm-4">
                  <label for="exchange-money">Tiền</label>
                  <input type="text" class="form-control" id="exchange-money" value="500,000VNĐ" readonly>
                  <span class="expression-bt">Tỉ lệ quy đổi: 1 điểm = 10,000VNĐ</span>
                </div>
                <div class="form-group has-icon col-sm-4">
                  <label for="exchange-voucher">Voucher <span class="expression">(Giảm giá cho chuyến đi tiếp)</span></label>
                  <input type="text" class="form-control" id="exchange-voucher" value="750,000VNĐ" readonly>
                  <span class="expression-bt">Tỉ lệ quy đổi: 1 điểm = 25,000VNĐ</span>
                </div>
              </div>
              <div class="row text-center">
                <div class="form-group col-sm-12">
                  <button type="submit" class="btn-exchange">Đổi</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div id="agent-money" class="tab-pane" role="tabpanel" aria-labelledby="agent-money-tab">        
        <div class="container">
          <div class="row voucher-list">
            <div class="col-sm-4">
              <div class="voucher-item voucher-item-text">
                <div class="voucher-item-text-image">
                  <a href=""><img src="images/voucher1.png" class="img-fluid" alt=""></a>
                </div>
                <div class="voucher-item-text-content">
                  <h3><a href="" title="">Giảm 650,000VNĐ cho chuyến đi tiếp theo</a></h3>
                  <div class="voucher-time">Từ 26/05/2020 - 30/06/2020</div>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="voucher-item voucher-item-code">
                <div class="voucher-code-text">Mã voucher</div>
                <div class="voucher-code-image">
                  <img src="images/voucher2.png" alt="" class="img-fluid">
                </div>
                <div class="date-voucher">Hết hạn trong 10 ngày tới</div>
                <div class="quick-copy-code">Sao chép mã</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="agent-voucher" class="tab-pane" role="tabpanel" aria-labelledby="agent-voucher-tab">
        <div class="eduction-table statements table-responsive">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Mô tả</th>
                <th scope="col">Thời gian</th>
                <th scope="col">Điểm</th>
                <th scope="col">Số dư</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Đổi 100 điểm</td>
                <td>25/06/2020</td>
                <td>-220</td>
                <td>280</td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>Đổi 200 điểm</td>
                <td>28/06/2020</td>
                <td>-280</td>
                <td>0</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    $('.has-icon').click(function() {
      $('.has-icon').removeClass('exchange-choose');
      $(this).addClass('exchange-choose');
    })
  });
</script>
<?php include 'footer.php'?>