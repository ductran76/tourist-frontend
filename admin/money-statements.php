<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Sao kê tiền</h2>
    <div class="">
     <div class="eduction-table statements table-responsive">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">STT</th>
            <th scope="col">Mô tả</th>
            <th scope="col">Thời gian</th>
            <th scope="col">Điểm</th>
            <th scope="col">Số dư</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Đổi 200 điểm</td>
            <td>28/06/2020</td>
            <td>-280</td>
            <td>0</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
          <tr>
            <th scope="row">4</th>
            <td>Đổi 200 điểm</td>
            <td>28/06/2020</td>
            <td>-280</td>
            <td>0</td>
          </tr>
          <tr>
            <th scope="row">5</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
          <tr>
            <th scope="row">6</th>
            <td>Đổi 200 điểm</td>
            <td>28/06/2020</td>
            <td>-280</td>
            <td>0</td>
          </tr>
          <tr>
            <th scope="row">7</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
          <tr>
            <th scope="row">8</th>
            <td>Đổi 200 điểm</td>
            <td>28/06/2020</td>
            <td>-280</td>
            <td>0</td>
          </tr>
          <tr>
            <th scope="row">9</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
          <tr>
            <th scope="row">10</th>
            <td>Đổi 200 điểm</td>
            <td>28/06/2020</td>
            <td>-280</td>
            <td>0</td>
          </tr>
          <tr>
            <th scope="row">11</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
          <tr>
            <th scope="row">12</th>
            <td>Đổi 200 điểm</td>
            <td>28/06/2020</td>
            <td>-280</td>
            <td>0</td>
          </tr>
          <tr>
            <th scope="row">13</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
          <tr>
            <th scope="row">14</th>
            <td>Đổi 200 điểm</td>
            <td>28/06/2020</td>
            <td>-280</td>
            <td>0</td>
          </tr>
           <tr>
            <th scope="row">15</th>
            <td>Đổi 100 điểm</td>
            <td>25/06/2020</td>
            <td>-220</td>
            <td>280</td>
          </tr>
        </tbody>
      </table>
      <div class="statistic d-flex align-items-center">
        <div class="mr-auto">
          <div class="statistic-title">Tổng: 205 kết quả</div>
        </div>
        <div class="statistic-pagination">
          <nav>
            <ul class="pagination d-flex justify-content-center flex-wrap pagination-flat pagination-success mb-0">
              <li class="page-item"><a class="page-link" href="#" data-abc="true"><i class="fa fa-angle-left"></i></a></li>
              <li class="page-item active"><a class="page-link" href="#" data-abc="true">1</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">2</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">3</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">4</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true"><i class="fa fa-angle-right"></i></a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php include 'footer.php'?>