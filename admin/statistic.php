<?php include 'sidebar.php'?>
<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js'></script>
<script>
  $(document).ready(function() {
    var ctx = $("#total-tour");
    var revenue = $("#revenue");
    var totalrevenue = $("#all-revenue");
    var myLineChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: ["Tour đã đi", "Tour sắp tới"],
        datasets: [{
          data: [8, 4],
          backgroundColor: ["#1171D3", "#DFEDFC"]
        }]
      }
    });
    var myLineChart2 = new Chart(revenue, {
      type: 'doughnut',
      data: {
        labels: ["Đã thu", "Ước tính"],
        datasets: [{
          data: [60, 36],
          backgroundColor: ["#F89B33", "#FFDDA7"]
        }]
      }
    });
    var myLineChart3 = new Chart(totalrevenue, {
     type: "bar",
     data: {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [
      {
        label: ["Doanh thu tháng"],
        data: [25, 32, 28, 22, 24, 40, 18, 36, 22, 38, 35, 28],
        fill: false,
        backgroundColor: ["#1171D3", "#1171D3", "#1171D3","#1171D3", "#1171D3", "#1171D3", "#1171D3", "#1171D3", "#1171D3", "#1171D3", "#1171D3", "#1171D3"]
      }]
    }
  });
  });
</script>
<div id="content">
  <div class="agent-page-title">
    <h2>Thống kê</h2>
    <div class="statistic-table">
     <div class="container">
       <div class="row">
         <div class="col-sm-6">
           <div class="statistic-item">
            <h2>Số tour tháng 8</h2>
            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
              <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
              </div>
              <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
              </div>
            </div> 
            <canvas id="total-tour" width="299" height="200" class="chartjs-render-monitor" style="display: block; width: 299px; height: 200px;"></canvas>
          </div>
        </div>

        <div class="col-sm-6">
         <div class="statistic-item">
          <h2>Doanh thu tháng 8 (đơn vị tính: triệu đồng)</h2>
          <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
            <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
              <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
            </div>
            <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
              <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
            </div>
          </div> 
          <canvas id="revenue" width="299" height="200" class="chartjs-render-monitor" style="display: block; width: 299px; height: 200px;"></canvas>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="all-revenue">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h2>Doanh thu</h2>
            </div>
            <div class="form-group-icon-calendar">
              <select id="year-number" class="form-control" required="">
                <option selected="">Năm 2019</option>
                <option>Năm 2018</option>
                <option>Năm 2017</option>
              </select>
            </div>
          </div>
          <div class="total-revenue">Tổng doanh thu: <span class="revenue-number">217 triệu</span></div>
          <canvas id="all-revenue" width="400" height="400"></canvas>
          <div class="caption">Biểu đồ doanh thu năm 2020</div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<?php include 'footer.php'?>