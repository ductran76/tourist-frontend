<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Hồ sơ của bạn</h2>

    <form id="msform" action="" method="POST">
      <!-- progressbar -->
      <ul id="progressbar">
        <li class="active">Hồ sơ</li>
        <li>Bài test</li>
        <li>Phỏng vấn</li>
      </ul>
      <!-- fieldsets -->
      <fieldset>
        <div class="profile-content">
          <div class="avatar-box">
            <div class="agent-avatar">
              <img src="images/profile.png" class="img-fluid" alt="">
              <div class="change-avatar">
                <input type="file" name="file" id="file-input" class="visuallyhidden">
                <i class="ion-ios-camera-outline file-upload"></i>
              </div>
            </div>
            <div class="avatar-text">Ảnh đại diện</div>
          </div>
          <div class="infomation-form">
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="surname">Họ</label>
                <input type="text" class="form-control" id="surname" value="Lương">
              </div>
              <div class="form-group col-sm-6">
                <label for="agent-name">Tên</label>
                <input type="text" class="form-control" id="agent-name" value="Xuân Phước">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="country">Quốc tịch</label>
                <select id="country" class="form-control">
                  <option selected="">Việt Nam</option>
                  <option>Thailand</option>
                  <option>China</option>
                  <option>Australia</option>
                </select>
              </div>
              <div class="form-group col-sm-6">
                <label for="agent-gender">Giới tính</label>
                <input type="text" class="form-control" id="agent-gender" value="Nam">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="agent-email">Email</label>
                <input type="email" class="form-control" id="agent-email" value="phuoclx129@gmail.com">
              </div>
              <div class="form-group col-sm-6">
                <label for="agent-phone">Số điện thoại</label>
                <input type="number" class="form-control" id="agent-phone" value="0973950482">
              </div>
            </div>
            <div class="row d-flex align-items-end">
              <div class="form-group col-sm-6">
                <label for="agent-cmt">Số CMT</label>
                <input type="number" class="form-control" id="agent-cmt" value="123456789">
              </div>
              <div class="form-group col-sm-6 custom-file-upload">
                <label for="file-upload-cmt" class="custom-file-upload1">
                  <i class="fa fa-file-image-o"></i> Cập nhật ảnh CMT (2 mặt)
                </label>
                <input id="file-upload-cmt" type="file"/>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="agent-introduce">Giới thiệu bản thân</label>
                <textarea type="text" class="form-control free-height" rows="5" id="agent-introduce" placeholder="Bạn là ai? Làm hướng dẫn viên trong bao lâu?.."></textarea>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="agent-price-day">Giá đi tour ngày thường(VNĐ)</label>
                <input type="text" class="form-control" id="agent-price-day" value="500,000">
              </div>
              <div class="form-group col-sm-6">
                <label for="agent-price-dayoff">Giá đi tour ngày lễ(VNĐ)</label>
                <input type="text" class="form-control" id="agent-price-dayoff" value="800,000">
              </div>
            </div>
            <div class="row d-flex align-items-end">
              <div class="form-group col-sm-6">
                <label for="agent-vehicle-type">Loại xe</label>
                <select id="agent-vehicle-type" class="form-control">
                  <option selected="">Ô tô</option>
                  <option>Xe máy</option>
                  <option>Xe bus</option>
                </select>
              </div>
              <div class="form-group col-sm-6 custom-file-upload">
                <label for="upload-vehicle-type" class="custom-file-upload1">
                  <i class="fa fa-file-image-o"></i> Cập nhật ảnh xe
                </label>
                <input id="upload-vehicle-type" type="file"/>
              </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <input type="button" name="next" class="next agent-update" value="Tiếp theo"/>
        </div>
      </fieldset>

      <fieldset>
        <div class="testing-question">
          <div class="question-item">
            <h3 class="question-title">1. Bạn đã có kinh nghiệm hướng dẫn viên trong bao lâu?</h3>
            <div class="choose-experience"> 
              <label id="experience1"> <input name="experience" type="radio"> <span>Dưới 3 năm</span> </label>
              <label id="experience2"> <input name="experience" type="radio"> <span>Từ 3 - 5 năm</span> </label>
              <label id="experience3"> <input name="experience" type="radio"> <span>Từ 5 - 8 năm</span> </label>
              <label id="experience4"> <input name="experience" type="radio"> <span>Trên 8 năm</span> </label>
            </div>
          </div>
          <div class="question-item">
            <h3 class="question-title">2. Các ngôn ngữ bạn có thể giao tiếp?</h3>
            <div class="infomation-form">
              <div class="form-group">
                <input type="text" class="form-control" id="" value="Tiếng Anh, Tiếng Trung">
              </div>
            </div>
          </div>
          <div class="question-item">
            <h3 class="question-title">3. Hãy mô tả một trường hợp bạn làm khách hàng bực bội và cách bạn xử lý vấn đề khi đó?</h3>
            <div class="infomation-form">
              <div class="form-group">
                <textarea type="text" class="form-control free-height" rows="3" id="" placeholder="Bạn sẽ làm gì?"></textarea>
              </div>
            </div>
          </div>
          <div class="question-item">
            <h3 class="question-title">4. Bạn biết gì về công ty chúng tôi? Nguồn thông tin là từ đâu vậy?</h3>
            <div class="infomation-form">
              <div class="form-group">
                <textarea type="text" class="form-control free-height" rows="3" id="" placeholder="Câu trả lời của bạn..."></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <input type="button" name="next" class="next agent-update" value="Tiếp theo"/>
        </div>
      </fieldset>

      <fieldset>
        <div class="last-question">
          <div class="last-question-image">
            <img src="images/question.png" class="img-fluid" alt="">
          </div>
          <h3>Cảm ơn đã đăng ký trở thành hướng dẫn viên của GuideStore!</h3>
          <div class="time-contact">Trong thời gian <strong>24h</strong> tới, GuideStore sẽ liên hệ để đặt lịch phỏng vấn với bạn (qua số điện thoại hoặc email đã đăng ký trong hồ sơ). Đừng quên kiểm tra mail và cuộc gọi của GuideStore nhé.</div>
        </div>
      </fieldset>
    </form>
  </div>
</div>

<script type="text/javascript">

  var current_fs, next_fs, previous_fs; 
  var left, opacity, scale; 
  var animating;

  $(".next").click(function(){
    if(animating) return false;
    animating = true;

    current_fs = $(this).parent().parent();
    next_fs = $(this).parent().parent().next();
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    next_fs.show(); 
    current_fs.animate({opacity: 0}, {
      step: function(now, mx) {
        scale = 1 - (1 - now) * 0.2;
        left = (now * 50)+"%";
        opacity = 1 - now;
        current_fs.css({
          'transform': 'scale('+scale+')',
          'position': 'absolute'
        });
        next_fs.css({'left': left, 'opacity': opacity});
      }, 
      duration: 100, 
      complete: function(){
        current_fs.hide();
        animating = false;
      }, 
      easing: ''
    });
  });

  // $(".previous").click(function(){
  //   if(animating) return false;
  //   animating = true;

  //   current_fs = $(this).parent();
  //   previous_fs = $(this).parent().prev();

  //   $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
  //   previous_fs.show(); 
  //   current_fs.animate({opacity: 0}, {
  //     step: function(now, mx) {
  //       scale = 0.8 + (1 - now) * 0.2;
  //       left = ((1-now) * 50)+"%";
  //       opacity = 1 - now;
  //       current_fs.css({'left': left});
  //       previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
  //     }, 
  //     duration: 800, 
  //     complete: function(){
  //       current_fs.hide();
  //       animating = false;
  //     }, 
  //     easing: ''
  //   });
  // });

  $(".submit").click(function(){
    return false;
  });
</script>

<?php include 'footer.php'?>