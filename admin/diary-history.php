<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Nhật kí tour</h2>
    <div class="diary-table-box">
      <button class="btn-creatdiary" data-izimodal-open="#creat-diary" data-izimodal-transitionin="comingIn"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Đăng bài</button>
      <div class="eduction-table diary-table table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">STT</th>
              <th scope="col">Tiêu đề</th>
              <th scope="col">Ngày cuối cập nhật</th>
              <th scope="col">Hành động</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td><a href="">LỊCH TRÌNH CHƠI BỜI Ở BUÔN MÊ THUỘT</a></td>
              <td>13/05/2020 - 23:59</a></td>
              <td>
                <a class="edit-diary" href="" data-izimodal-open="#edit-diary" data-izimodal-transitionin="comingIn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a class="delete-diary" href="" data-izimodal-open="#delete-diary" data-izimodal-transitionin="comingIn"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              </td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td><a href="">LỚN RỒI, THÈM GÌ ĂN NẤY, THÍCH GÌ THÌ MUA</a></td>
              <td>25/06/2020 - 22:34</td>
              <td>
                <a class="edit-diary" href="" data-izimodal-open="#edit-diary" data-izimodal-transitionin="comingIn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a class="delete-diary" href="" data-izimodal-open="#delete-diary" data-izimodal-transitionin="comingIn"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              </td>
            </tr>
          </tbody>
        </table>
        <div class="statistic d-flex align-items-center">
        <div class="mr-auto">
          <div class="statistic-title">Tổng: 2 bài viết</div>
        </div>
        <div class="statistic-pagination">
          <nav>
            <ul class="pagination d-flex justify-content-center flex-wrap pagination-flat pagination-success mb-0">
              <li class="page-item"><a class="page-link" href="#" data-abc="true"><i class="fa fa-angle-left"></i></a></li>
              <li class="page-item active"><a class="page-link" href="#" data-abc="true">1</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">2</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">3</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">4</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true"><i class="fa fa-angle-right"></i></a></li>
            </ul>
          </nav>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

<div id="creat-diary" class="modal-izi"> 
  <button data-izimodal-close="" class="icon-close" data-izimodal-transitionout="comingOut"></button>
  <div class="diary-content">
    <h2 class="title-pop text-left">Tạo bài viết</h2>
    <div class="form-diary">
      <form class="" action="" method="POST">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Tiêu đề bài viết..." id="title-diary" required="">
        </div>
        <div class="form-group">
          <textarea type="text" class="form-control" rows="10" required placeholder="Nội dung bài viết..."></textarea>
        </div>
        <div class="form-group text-right">
          <button type="submit" class="btn-creatdiary btn-static"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Đăng bài</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="edit-diary" class="modal-izi"> 
  <button data-izimodal-close="" class="icon-close" data-izimodal-transitionout="comingOut"></button>
  <div class="diary-content">
    <h2 class="title-pop text-left">Chỉnh sửa bài viết</h2>
    <div class="form-diary">
      <form class="" action="" method="POST">
        <div class="form-group">
          <input type="text" class="form-control" value="LỊCH TRÌNH CHƠI BỜI Ở BUÔN MÊ THUỘT" id="title-diary">
        </div>
        <div class="form-group">
          <textarea type="text" class="form-control" rows="10">
            Hồi nhỏ, mình thích vòi bố mẹ tiền quà bánh, mua cho món đồ ghép hình đắt xíu tầm 100k nếu được học sinh giỏi học kỳ.
            Lúc thi đậu đại học, lại lấy lý do đó mà vòi mẹ tiền đề mua cái laptop đi học. Lớn lên đi làm, khi được công việc mới, mình dành tháng lương đầu dẫn mẹ đi ăn.
            Thắng được dự án lớn, mình dẫn mẹ đi du lịch.
            Ngày nào thấy mình làm tốt, tự thưởng mình một món tráng miệng đắt tiền. Lúc nào dư tiền, tự động nâng cấp laptop, mua thêm máy chụp ảnh mình cần.
            Lớn rồi, thèm gì thì mua ăn, thích ai thì tỏ tình, cần gì thì mua sắm miễn trong khả năng.
            Lớn rồi, cũng không cần dựa vào ai để làm mình hạnh phúc, vì hạnh phúc đến từ chính mình và sẽ lan toả ra người xung quanh.
            Lớn rồi, có khóc thì lau nước mắt, chán đời thì lên liền một chuyến xe đi Đà Lạt ngủ, chán thì về.
            Lớn rồi, mình cứ sống thôi!
          </textarea>
        </div>
        <div class="form-group text-right">
          <button type="submit" class="btn-creatdiary btn-static"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Đăng bài</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="delete-diary" class="modal-izi"> 
  <button data-izimodal-close="" class="icon-close" data-izimodal-transitionout="bounceOutDown"></button>
  <div class="diary-content text-center">
    <div class="delete-icon">
      <i class="fa fa-trash-o" aria-hidden="true"></i>
    </div>
    <div class="confirm-delete">Xoá bài viết?</div>
    <div class="title-diary-del">LỊCH TRÌNH CHƠI BỜI Ở BUÔN MÊ THUỘT</div>
    <footer>
      <button class="btn-cancel" data-izimodal-close="" data-izimodal-transitionout="bounceOutDown">Hủy</button>
      <button class="btn-delete">Xóa</button>
    </footer>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $("#creat-diary, #edit-diary, #delete-diary").iziModal({
      padding: 25
    });
    $("#creat-diary").iziModal('setWidth', 950);
    $("#edit-diary").iziModal('setWidth', 950);
    $("#delete-diary").iziModal('setWidth', 385);
  })
</script>
<?php include 'footer.php'?>