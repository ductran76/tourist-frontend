<?php include 'sidebar.php'?>

<div id="content">
  <div class="agent-page-title">
    <h2>Lịch sử tour</h2>
    <div class="">
     <div class="eduction-table table-responsive">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">STT</th>
            <th scope="col">Điểm đón</th>
            <th scope="col">Điểm đến</th>
            <th scope="col">Thời gian</th>
            <th scope="col">Chi phí</th>
            <th scope="col">Điểm</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Hà Nội</td>
            <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
            <td>13/05/2020 - 16/05/2020</td>
            <td>2,210,000VNĐ</td>
            <td>+220</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Hà Nội</td>
            <td><span class="destination">Đà Lạt</span> <span class="destination">TP HCM</span></td>
            <td>21/01/2020 - 26/01/2020</td>
            <td>3,300,000VNĐ</td>
            <td>+330</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Hà Nội</td>
            <td><span class="destination">Quy Nhơn</span></td>
            <td>16/07/2019 - 20/07/2020</td>
            <td>2,400,000VNĐ</td>
            <td>+240</td>
          </tr>
          <tr>
            <th scope="row">4</th>
            <td>Hà Nội</td>
            <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
            <td>13/05/2020 - 16/05/2020</td>
            <td>2,210,000VNĐ</td>
            <td>+220</td>
          </tr>
          <tr>
            <th scope="row">5</th>
            <td>Hà Nội</td>
            <td><span class="destination">Đà Lạt</span> <span class="destination">TP HCM</span></td>
            <td>21/01/2020 - 26/01/2020</td>
            <td>3,300,000VNĐ</td>
            <td>+330</td>
          </tr>
          <tr>
            <th scope="row">6</th>
            <td>Hà Nội</td>
            <td><span class="destination">Quy Nhơn</span></td>
            <td>16/07/2019 - 20/07/2020</td>
            <td>2,400,000VNĐ</td>
            <td>+240</td>
          </tr>
          <tr>
            <th scope="row">7</th>
            <td>Hà Nội</td>
            <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
            <td>13/05/2020 - 16/05/2020</td>
            <td>2,210,000VNĐ</td>
            <td>+220</td>
          </tr>
          <tr>
            <th scope="row">8</th>
            <td>Hà Nội</td>
            <td><span class="destination">Đà Lạt</span> <span class="destination">TP HCM</span></td>
            <td>21/01/2020 - 26/01/2020</td>
            <td>3,300,000VNĐ</td>
            <td>+330</td>
          </tr>
          <tr>
            <th scope="row">9</th>
            <td>Hà Nội</td>
            <td><span class="destination">Quy Nhơn</span></td>
            <td>16/07/2019 - 20/07/2020</td>
            <td>2,400,000VNĐ</td>
            <td>+240</td>
          </tr>
          <tr>
            <th scope="row">10</th>
            <td>Hà Nội</td>
            <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
            <td>13/05/2020 - 16/05/2020</td>
            <td>2,210,000VNĐ</td>
            <td>+220</td>
          </tr>
          <tr>
            <th scope="row">11</th>
            <td>Hà Nội</td>
            <td><span class="destination">Đà Lạt</span> <span class="destination">TP HCM</span></td>
            <td>21/01/2020 - 26/01/2020</td>
            <td>3,300,000VNĐ</td>
            <td>+330</td>
          </tr>
          <tr>
            <th scope="row">12</th>
            <td>Hà Nội</td>
            <td><span class="destination">Quy Nhơn</span></td>
            <td>16/07/2019 - 20/07/2020</td>
            <td>2,400,000VNĐ</td>
            <td>+240</td>
          </tr>
          <tr>
            <th scope="row">13</th>
            <td>Hà Nội</td>
            <td><span class="destination">Hà Nội</span> <span class="destination">Ninh Bình</span></td>
            <td>13/05/2020 - 16/05/2020</td>
            <td>2,210,000VNĐ</td>
            <td>+220</td>
          </tr>
          <tr>
            <th scope="row">14</th>
            <td>Hà Nội</td>
            <td><span class="destination">Đà Lạt</span> <span class="destination">TP HCM</span></td>
            <td>21/01/2020 - 26/01/2020</td>
            <td>3,300,000VNĐ</td>
            <td>+330</td>
          </tr>
          <tr>
            <th scope="row">15</th>
            <td>Hà Nội</td>
            <td><span class="destination">Quy Nhơn</span></td>
            <td>16/07/2019 - 20/07/2020</td>
            <td>2,400,000VNĐ</td>
            <td>+240</td>
          </tr>
        </tbody>
      </table>
      <div class="statistic d-flex align-items-center">
        <div class="mr-auto">
          <div class="statistic-title">Tổng: 205 (tour)</div>
        </div>
        <div class="statistic-pagination">
          <nav>
            <ul class="pagination d-flex justify-content-center flex-wrap pagination-flat pagination-success mb-0">
              <li class="page-item"><a class="page-link" href="#" data-abc="true"><i class="fa fa-angle-left"></i></a></li>
              <li class="page-item active"><a class="page-link" href="#" data-abc="true">1</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">2</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">3</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true">4</a></li>
              <li class="page-item"><a class="page-link" href="#" data-abc="true"><i class="fa fa-angle-right"></i></a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php include 'footer.php'?>